package com.cucumber.frame.ui.hooks;

import com.cucumber.frame.ui.driver.DriverManager;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.qameta.allure.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.ByteArrayInputStream;

import static com.cucumber.frame.ui.driver.DriverManager.getDriver;

public class DriverHooks {
    @Before
    public void setupDriver(Scenario scenario) {
        if (scenario.getSourceTagNames().contains("@UI"))
            DriverManager.setUpDriver();
    }

    @After
    public void saveScreenshot(Scenario scenario) {
        if (scenario.isFailed() && scenario.getSourceTagNames().contains("@UI")) {
            Allure.addAttachment("Any Name", new ByteArrayInputStream(((TakesScreenshot) getDriver())
                    .getScreenshotAs(OutputType.BYTES)));
        }
        if (scenario.getSourceTagNames().contains("@UI"))
            DriverManager.quitDriver();
    }
}
